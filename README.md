# Rcc Push Server

This setup needs review.

This service registers future push notifications and triggers them at the appropriate time.


Start with `npm start`

## Configuration

The service expects a file `config.json` to run with the following content:

```
{
	"port": 		9050, // the port this service runs on
	"publicKey": 	"<public key>", // see vapidkey for web-push
	"privateKey": 	"<private key>", // see vapidkey for web-push
	"email":		"<email>"  // mail for 3rd parties (like mozilla, google) to contact in case this service goes rampant
}
```

## Storage

This service stores all registered push notification request in a zip file. 
That's far from great, but for the time beeing it does not need a special data base service.

Will ignore notifications schedule for the past or more than 7 days into the future.
