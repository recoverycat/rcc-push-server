import config from './config.json' assert {type: "json"}
import webPush from 'web-push'
import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import {ReminderService} from './reminder-service.js'


async function setup() {

    const app = express()

    var corsOptions = {
        origin: '*',
        allowedHeaders: ['Content-Type', 'Authorization']
    }

    app.use(cors(corsOptions))

    app.use(bodyParser.json())

    if (!config.publicKey || !config.privateKey) {
        console.log("Missing keys. You can use the following ones:")
        console.log(webPush.generateVAPIDKeys())
        process.exit(1)
    }

    if (!config.email) {
        console.log("Missing config.email.")
        process.exit(1)
    }

    webPush.setVapidDetails(
        `mailto:${config.mail}`,
        config.publicKey,
        config.privateKey
    )

    const reminderService = new ReminderService(config, webPush, 'timeouts.json.gz')

    app.get('/vapidPublicKey', (req, res) => reminderService.publicKeyHandler(req, res))
    app.post('/register', (req, res) => reminderService.registrationHandler(req, res))

    await reminderService.restoreFromFile()

    app.listen(config.port, () => {
        console.log(`Push server listening on port ${config.port}!`)
    })

}

setup()