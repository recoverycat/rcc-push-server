import {
    gzip,
    gunzip
} from 'zlib'
import {
    writeFile,
    readFile,
    existsSync
} from 'fs'

export class ReminderService {

    items = []
    backupScheduled = false
    backupOngoing = false
    backupFile = undefined

    constructor(config, webPush, filename) {
        this.config = config
        this.webPush = webPush
        this.backupFile = 'backup/' + filename

        if (!filename) throw "ReminderService.constructor(): missing filename."
    }

    async restoreFromFile() {

        if (!this.backupFile) throw "ReminderService.restoreFromFile(): missing backup file."

        const backup_exists = existsSync(this.backupFile)

        if (!backup_exists) {
            console.warn("ReminderService.restoreFromFile(): backup does not exist	")
            return []
        }

        console.info("ReminderService.restoreFromFile(): backup exists")

        const zipped = await new Promise((resolve, reject) => readFile(this.backupFile, (error, data) => error ? reject(error) : resolve(data)))
        const unzipped = await new Promise((resolve, reject) => gunzip(zipped, (error, data) => error ? reject(error) : resolve(data)))

        this.items = JSON.parse((unzipped.toString()))

        this.cleanItems()

        this.advanceItems()

        setInterval(() => this.scheduleBackup(), 1000 * 60)

    }


    async scheduleBackup() {

        this.backupsScheduled = true

        if (this.backupOngoing) return null

        await new Promise(resolve => setTimeout(resolve, 1000 * 20))

        if (this.backupOngoing) return null

        this.backupOngoing = true

        try {

            this.cleanItems()

            const buffer = Buffer.from(JSON.stringify(this.items.map(({subscription, reminders}) => ({
                subscription,
                reminders
            }))))

            this.backupsScheduled = false

            const zipped = await new Promise((resolve, reject) => gzip(buffer, (error, data) => error ? reject(error) : resolve(data)))

            await new Promise((resolve, reject) => writeFile(this.backupFile, zipped, (error, data) => error ? reject(error) : resolve(data)))

            this.backupOngoing = false

        } catch (e) {

            this.backupOngoing = false

            throw e

        }


        if (this.backupsScheduled) this.scheduleBackup()

    }


    async notify(subscription, payload) {

        if (!subscription) {
            console.log('ReminderService.notify(): missing subscription.')
            return null
        }

        payload = payload || null

        const options = {}

        //console.log(new Date(), 'Notifying:', subscription.endpoint)

        try {
            await this.webPush.sendNotification(subscription, payload, options)
        } catch (e) {
            console.log(e)
        }

    }

    getReminderObject(reminder) {

        if (typeof reminder == 'number') return {id: undefined, timestamp: reminder}

        if (!reminder) return reminderError = 'reminder must be a number or an object.'

        const {id, timestamp} = reminder

        const idOkay = id === undefined || typeof id == 'string'
        const tsOkay = typeof timestamp == 'number'

        if (!idOkay) return {error: 'reminder.id must be undefined or a string.'}
        if (!tsOkay) return {error: 'reminder.timestamp must be a number.'}

        return {id, timestamp}
    }

    getCleanItem(endpoint) {

        const pos = this.items.findIndex(item => item.subscription && (item.subscription.endpoint == endpoint))

        if (pos === -1) return undefined

        const current = this.items[pos]

        if (!current) return undefined

        const now = Date.now()

        current.reminders = (current.reminders || [])
            .map(reminder => this.getReminderObject(reminder))
            .filter(({timestamp}) => timestamp >= now && timestamp - now <= 1000 * 60 * 60 * 24 * 7)
            .sort((a, b) => a.timestamp > b.timestamp ? 1 : -1)


        if (current.reminders.length === 0) {
            this.items.splice(pos, 1)
            return undefined
        }

        return current
    }

    cleanItems() {
        this.items.forEach(item => this.getCleanItem(item.subscription.endpoint))
    }


    clearItem(endpoint) {

        const current = this.getCleanItem(endpoint)

        if (!current) return false
        if (!current.id) return false

        clearTimeout(current.id)

        delete current.id

        return true
    }


    advanceItems() {
        this.items.forEach(item => this.advanceItem(item.subscription.endpoint))
    }

    advanceItem(endpoint) {

        const current = this.getCleanItem(endpoint)

        if (!current) return null
        if (!current.reminders) return null
        if (!current.reminders[0]) return null

        const now = Date.now()
        const upcoming = current.reminders[0].timestamp
        const delay = upcoming - now

        this.clearItem(endpoint)

        //console.log(new Date(), 'advanceItem:', delay/1000/60, 'minutes', endpoint)

        current.id = setTimeout(
            () => Promise.resolve()
                .then(() => this.notify(current.subscription))
                .then(() => this.advanceItem(endpoint))
                .catch(console.log)
            , delay)

        this.scheduleBackup()

        return true
    }

    /**
     * Merges two arrays of reminder objects. All of the second one will enter the result.
     * The reminder objects of the first one will only be added, if they do not conflict with
     * another reminder.
     */
    mergeReminders(remindersA, remindersB) {

        remindersA = remindersA || []
        remindersB = remindersB || []

        const result = [...remindersB]

        remindersA.forEach(reminderA => {

            reminderA = this.getReminderObject(reminderA)

            const conflictingReminder = result.find(reminderB => {

                reminderB = this.getReminderObject(reminderB)

                const sameId = reminderA.id === reminderB.id

                if (!sameId) return false								// Different IDs

                const sameTimestamp = reminderA.timestamp == reminderB.timestamp

                return reminderA.id
                    ? true			// With IDs and they are the same
                    : sameTimestamp	// Without IDs but same timestamp

            })


            if (!conflictingReminder) result.push(reminderA)

        })

        return result
    }


    updateItem(subscription, reminders) {

        const endpoint = subscription && subscription.endpoint

        if (!endpoint) {
            console.log('updateItem(): Missing subscription.endpoint.')
            return false
        }

        const now = Date.now()

        let current = this.getCleanItem(endpoint)

        if (!current) {
            current = {subscription}
            this.items.push(current)
        }

        current.reminders = this.mergeReminders(current.reminders, reminders)

        this.advanceItem(endpoint)

        this.scheduleBackup()

        return current

    }

    publicKeyHandler(req, res) {
        res.send(this.config.publicKey)
    }


    registrationHandler(req, res) {

        let {subscription, reminders} = req.body

        //console.log(new Date(), 'Received new registration:', reminders, subscription.endpoint)

        if (!reminders) {
            res.sendStatus(400, 'missing .reminders')
            return false
        }

        if (!Array.isArray(reminders)) {
            res.sendStatus(400, 'reminders must be an array')
            return false
        }

        reminders = reminders.map(reminder => this.getReminderObject(reminder))

        const badReminder = reminders.find(reminder => reminder.error)

        if (badReminder) {
            res.sendStatus(400, badReminder.error)
            return false
        }

        const current = this.updateItem(subscription, reminders)

        //console.log(new Date(), 'New item:', current.subscription.endpoint, current.reminders)

        if (!current) {
            res.sendStatus(400)
            return false
        }

        res.sendStatus(200)

    }

}



















