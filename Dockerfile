FROM node:20.18-alpine

# Create app directory
WORKDIR /usr/src/app

# Create backup folder
RUN mkdir backup && chown -R 1000:2000 backup && chmod -R 775 backup

# Install dependencies
COPY package*.json ./
RUN npm ci --only=production

# Copy everything
COPY . .

# Expose port
EXPOSE 9050

CMD ["npm","run","start"]
